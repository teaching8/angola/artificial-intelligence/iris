from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.svm import SVC


'''
Retorna a precisão da previsão.

Parâmetros:
     previsão (array): valores de previsão
     real (array): valores reais

Retorna:
     (float): a proporção de observações classificada corretamente.
'''
def accuracy_score(prediction, real):
    pass


def main():
    # Carregar conjunto de dados Iris
    X, y = load_iris(return_X_y=True)

    # Dividir o conjunto de dados em conjuntos de treinamento e teste
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

    # Treinamento
    model = SVC()
    model.fit(X_train, y_train)

    prediction = model.predict(X_test)
    accuracy = accuracy_score(prediction, y_test)

    print(f'accurancy: {accuracy}')


if __name__ == '__main__':
    main()
