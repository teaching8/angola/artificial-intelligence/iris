import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import make_classification
from sklearn.dummy import DummyClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import pandas as pd

def main():
    # Generate features matrix and target vector
    X, y = make_classification(
        n_samples = 297,
        n_features = 8,
        n_informative = 8,
        n_redundant = 0,
        n_classes = 2,
        weights = (0.90,),
        random_state = 1
    )
    class_names = ['A', 'B']

    # Create training and test set
    features_train, features_test, target_train, target_test = train_test_split(
        X, y, random_state=1
    )

    ## Dummy classifier
    classifier = DummyClassifier(strategy='stratified', random_state=1)
    # Cross-validate model using accuracy
    score = cross_val_score(classifier, X, y, scoring="accuracy")
    print(f'Accuracy: {score}')
    # Train model and make predictions
    target_predicted = classifier.fit(
        features_train,
        target_train
    ).predict(features_test)

    # Create confusion matrix
    matrix = confusion_matrix(target_test, target_predicted)
    # Create pandas dataframe
    dataframe = pd.DataFrame(matrix, index=class_names, columns=class_names)
    # Create heatmap
    sns.heatmap(dataframe, annot=True, cbar=None, cmap="Blues")
    plt.title("Dummy classifier"), plt.tight_layout()
    plt.ylabel("True Class"), plt.xlabel("Predicted Class")
    plt.show()

    # Create logistic regression
    classifier = LogisticRegression()
    # Cross-validate model using accuracy
    score = cross_val_score(classifier, X, y, scoring="accuracy")
    print(f'Accuracy: {score}')
    # Train model and make predictions
    target_predicted = classifier.fit(
        features_train,
        target_train
    ).predict(features_test)

    # Create confusion matrix
    matrix = confusion_matrix(target_test, target_predicted)
    # Create pandas dataframe
    dataframe = pd.DataFrame(matrix, index=class_names, columns=class_names)
    # Create heatmap
    sns.heatmap(dataframe, annot=True, cbar=None, cmap="Blues")
    plt.title("Logistic regression"), plt.tight_layout()
    plt.ylabel("True Class"), plt.xlabel("Predicted Class")
    plt.show()


if __name__ == '__main__':
    main()