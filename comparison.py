from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.datasets import load_iris
from sklearn import svm


def main():
    # Carregar conjunto de dados Iris
    X, y = load_iris(return_X_y=True)

    # Dividir o conjunto de dados em conjuntos de treinamento e teste
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

    svc = svm.SVC()
    linear = svm.LinearSVC()

    # Treinamento
    svc.fit(X_train, y_train)

    # Predicão
    svc_prediction = svc.predict(X_test)
    svc_accuracy = accuracy_score(svc_prediction, y_test)


if __name__ == '__main__':
    main()
